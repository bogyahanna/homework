package orszagok_list_set;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Orszagok_hashSet {

    public static void main(String[] args) {

        Set<City> citiesOfIsrael = new HashSet<>();
        citiesOfIsrael.add(new City("Jerusalem", 12));
        citiesOfIsrael.add(new City("TelAviv", 13));
        citiesOfIsrael.add(new City("BeerSheva", 9));
        citiesOfIsrael.add(new City("BneiBrak", 10));
        citiesOfIsrael.add(new City("BeerSheva", 9));
        System.out.println(citiesOfIsrael.size());

        Set<City> citiesOfRomania = new HashSet<>();
        citiesOfRomania.add(new City("Bucuresti", 40));
        citiesOfRomania.add(new City("Sibiu", 25));
        citiesOfRomania.add(new City("Constanta", 19));
        System.out.println(citiesOfRomania.size());

        Set<City> citiesOfSweden = new HashSet<>();
        citiesOfSweden.add(new City("Stockholm", 45));
        citiesOfSweden.add(new City("Göteborg", 40));
        citiesOfSweden.add(new City("Malmö", 30));
        citiesOfSweden.add(new City("Stockholm", 45));
        citiesOfSweden.add(new City("Göteborg", 40));
        citiesOfSweden.add(new City("Malmö", 30));
        System.out.println(citiesOfSweden.size());

        Country romania = new Country("Romania", citiesOfRomania); 
        Country israel = new Country("Israel", citiesOfIsrael);
        Country sweden = new Country("Sweden", citiesOfSweden);

        Set<Country> countries = new HashSet<>();
        countries.add(sweden);
        countries.add(israel);
        countries.add(romania);

        countries.add(israel);
        countries.add(israel);

        System.out.println("Countries size: " + countries.size());

        Set<Country> countriesTree = new TreeSet<>();
        
        countriesTree.add(sweden);      
        countriesTree.add(romania);
        countriesTree.add(israel);
        
      
        System.out.println(countriesTree.size());
        
        for (Country i : countriesTree) {
            System.out.println(i);
        }

    }
}
