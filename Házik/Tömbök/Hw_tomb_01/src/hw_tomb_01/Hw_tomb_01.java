/*
 1. A program dolgozzon egy 20 elemű tömbbel, töltse fel véletlen kétegyű
 számokkal! Minden részfeladatot külön metódusba kell megvalósítani
 - Listázza a tömbelemeket
 - Páros tömbelemek összege
 - Van-e öttel osztható szám
 - Melyik az első páratlan szám a tömbben
 - Van-e a tömbben 32

 */
package hw_tomb_01;

public class Hw_tomb_01 {

    public static void main(String[] args) {
        int[] array = new int[20];
        fillRandomNumbers(array);
        listItems(array);
        System.out.println("");
        System.out.println("A tömb páros elemeinek összege: " + sumEvens(array));

        System.out.println("Van öttel osztható elem: " + isDivisibleBy(array, 5));
        System.out.println("A tömb első páratlan eleme: " + firstOddItem(array));
        System.out.println("A tömb egyik eleme 32: " + isGivenNumberIn(array, 32));

    }

    public static void fillRandomNumbers(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * (99 - 10) + 10);
        }
    }

    public static void listItems(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ", ");
        }
    }

    public static int sumEvens(int[] array) {
        int sumEvens = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                sumEvens += array[i];
            }
        }
        return sumEvens;
    }

    public static boolean isDivisibleBy(int[] array, int div) {
        div = 5;
        boolean divisible = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % div == 0) {
                divisible = true;
                break;
            } else {
                divisible = false;
            }
        }
        return divisible;
    }

    public static int firstOddItem(int[] array) {
        int firstOddItem = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 1) {
                firstOddItem = array[i];
                break;
            }
        }
        return firstOddItem;
    }

    public static boolean isGivenNumberIn(int[] array, int number) {
        number = 32;
        boolean isNumberIn = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                isNumberIn = true;
                break;
            } else {
                isNumberIn = false;
            }
        }
        return isNumberIn;
    }
}
