/*
 fibonacci sorozat n. elemének kiszámolására, 
 oldja meg hagyományos és rekurzív módon
 */
package hw_tomb_05_fibonacci;

public class Hw_tomb_05_fibonacci {

    public static void main(String[] args) {
        printFibonacciNumber(30);

    }

    public static void printFibonacciNumber(int counter) {
        int number1 = 0;
        int number2 = 1;
        int number3;
        

        System.out.print(number1 + ", " + number2);

        for (int i = 2; i < counter; i++) {
            number3 = number1 + number2;
            System.out.print(", " + number3);
            number1 = number2;
            number2 = number3;
        }
    }

}
