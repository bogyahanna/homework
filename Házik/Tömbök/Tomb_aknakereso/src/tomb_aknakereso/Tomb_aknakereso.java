/*

 */
package tomb_aknakereso;

import java.util.Scanner;

public class Tomb_aknakereso {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int tippX;    //koordináta-tipp
        int tippY;      // koordináta-tipp

        int lepesSzamlalo = 0;
        int aknaSzamlalo = 0;

        boolean survive = false;

        int[][] field = new int[7][10];

        int areaField = 6 * 9;
        int numberOfBombs = (int) (Math.random() * ((areaField / 2 + 1) - areaField / 4) + areaField / 4);
        int bombCounter = 0;
        System.out.println("bombák: " + numberOfBombs);

        do {
            int x = (int) (Math.random() * 6); // bomba koordináták
            int y = (int) (Math.random() * 9); //bomba koordináták
            if (field[x][y] == 0) {
                field[x][y] = 1;
                bombCounter++;
            }

        } while (bombCounter < numberOfBombs);

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                field[0][j] = 0;
                field[i][0] = 0;
                field[field.length - 1][j] = 0;
                field[i][field[i].length - 1] = 0;
                System.out.print(field[i][j] + " ");
            }
            System.out.println("");
        }

        System.out.println("Induljon a tippelés, add meg a koordinátákat: ");

        do {
            tippX = sc.nextInt();
            if (tippX <= 6 && tippX > 0) {
                System.out.println("Rendben");
            } else {
                System.out.println("Ez pályán kívül ment, adj meg újat: ");
                tippX = sc.nextInt();
            }

            tippY = sc.nextInt();
            if (tippY <= 9 && tippY > 0) {
                System.out.println("Koordináta rögzítve");
            } else {
                System.out.println("Ez pályán kívül ment, adj meg újat: ");
                tippY = sc.nextInt();
            }

            if (field[tippX][tippY] == 1) {
                survive = false;
            } else if (field[tippX][tippY] == 0) {
                survive = true;

                if (field[tippX - 1][tippY] == 1) {
                    aknaSzamlalo++;
                }
                if (field[tippX - 1][tippY - 1] == 1) {
                    aknaSzamlalo++;
                }
                if (field[tippX][tippY - 1] == 1) {
                    aknaSzamlalo++;
                }
                if (field[tippX + 1][tippY - 1] == 1) {
                    aknaSzamlalo++;
                }
                if (field[tippX + 1][tippY] == 1) {
                    aknaSzamlalo++;
                }
                if (field[tippX + 1][tippY + 1] == 1) {
                    aknaSzamlalo++;
                }
                if (field[tippX - 1][tippY + 1] == 1) {
                    aknaSzamlalo++;
                }
                if (field[tippX][tippY + 1] == 1) {
                    aknaSzamlalo++;
                }

                switch (aknaSzamlalo) {
                    case 1:
                        System.out.println("Nincs para, csak 1 akna van körülötted");
                        break;
                    case 2:
                    case 3:
                    case 4:
                        System.out.println("Kezd melegedni a helyzet");
                        break;
                    case 5:
                    case 6:
                        System.out.println("Hurt Locker!");
                        break;
                }
                aknaSzamlalo = 0;
            }
            lepesSzamlalo++;

        } while (survive != false);
        System.out.println("Aknára léptél, felrobbantál!");
        System.out.println("Ennyi lépést sikerült robbanás nélkül megtenned: " + (lepesSzamlalo - 1));
    }
}
