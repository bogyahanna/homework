package zh2_feladat;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class DatePanel extends JFrame implements ActionListener {

    private final JPanel panel;
    private final JTextArea text;
    private final JButton button;
    private List<String> dates = new ArrayList<>();
    private File file = new File("dates.txt");
    private int numOfClicks;

    public DatePanel(String title) throws HeadlessException {
        super(title);
        panel = new JPanel();
        text = new JTextArea();
        button = new JButton("Click for date");

    }

    public void init() {
        basicSetup();
        textAreaSetup();
        buttonSetup();

    }

    public void basicSetup() {
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setTitle("Calculator");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        button.addActionListener(this);

        panel.setLayout(new GridBagLayout());
        add(panel);
    }

    public void textAreaSetup() {
        Font f = new Font("Arial", Font.BOLD, 15);
        this.text.setFont(f);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 4;
        gbc.weightx = 6;
        gbc.fill = GridBagConstraints.CENTER;

        panel.add(this.text, gbc);

    }

    public void buttonSetup() {
        numOfClicks = 0;
        
        GridBagConstraints gbc = new GridBagConstraints();
        
        gbc.gridx = 3;
        gbc.gridy = 3;
        gbc.gridwidth = 4;
        gbc.gridheight = 1;
        gbc.fill = GridBagConstraints.BOTH;

        panel.add(this.button, gbc);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        numOfClicks++;
        Date date = new Date();
        StringBuilder sb = new StringBuilder();

        if (e.getSource() == button) {

            text.append(numOfClicks + ": " + date.toString()  + ": Button pressed" + "\n");

            sb.append(text.getText() + "\n");
        }
        
        dateToString(sb);        
        writeClickDateInFile(sb);
    }

    public void dateToString(StringBuilder sb) {
        dates.add(sb.toString());
    }

    public void writeClickDateInFile(StringBuilder sb) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(this.file))) {
            bw.write(sb.toString());

        } catch (IOException ioe) {
            System.err.println("Hiba a fájl írása közben!");
        }
    }

}
