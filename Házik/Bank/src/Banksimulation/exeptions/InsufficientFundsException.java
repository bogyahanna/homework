
package Banksimulation.exeptions;

import Banksimulation.model.Transfer;

public class InsufficientFundsException extends BankingException {

    public InsufficientFundsException(Transfer transfer) {
        super("Nincs elĂ©g lĂłvĂ©!", transfer);
    }

}

