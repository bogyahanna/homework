
package dateprint.controller;

import dateprint.model.DatePrintModel;
import dateprint.view.DatePrintView;

public class DatePrintController {
    private DatePrintModel model;
    private DatePrintView view;

    public DatePrintController() {
        this.model = new DatePrintModel();
        this.view = new DatePrintView(this);
        this.view.init();
    }
    
    

    public DatePrintModel getModel() {
        return model;
    }

    public DatePrintView getView() {
        return view;
    }
    
    
    
    
}
