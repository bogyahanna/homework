/*
 A program olvassa be a „scanner_text.txt” fájl tartalmát és írja ki a konzolra.
 */
package file_gyakorlatok;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Forth {

    public static void main(String[] args) {

        File file = new File("scanner_text");

        try (BufferedReader buffR = new BufferedReader(new FileReader(file))) {
           String line = buffR.readLine();
            do {
                System.out.println(line);
                line = buffR.readLine();                    

            } while (line != null);

        } catch (IOException ioe) {
            System.out.println("nem talált");
        }

    }
}
