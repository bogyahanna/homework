/*
 Az előző feladatokat módosítsuk úgy, hogy a program minden egyes futásnál az épp 
 aktuális idő szerinti mappába teszi a szelvényeket, illetve az összes ilyen mappát
 olvassuk fel és írjuk ki a konzolra.
 */
package lottery_ticket;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import static lottery_ticket.Lottery_ticket.lotteryNumbersGenerator;

public class Third_lottery {

    public static void main(String[] args) throws FileNotFoundException {
        int howMany = scanUserInput();

        String depoPath = stringDateGenerator();
        
        /*SimpleDateFormat dateFormat = new SimpleDateFormat("hh mm ss");
         String time = dateFormat.format(now);*/
        
        File depo = new File(depoPath);
        depo.mkdir();

        writeTicketInMap(howMany);
        
        int counter = howManyFilesAre();
        
        readAndPrintFromFolder(counter);
    }

    public static void readAndPrintFromFolder(int counter) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < counter; i++) {
            try (BufferedReader buffR = new BufferedReader(new FileReader("lottery_ticket_" + i))) {
                String line = buffR.readLine();
                while (line != null) {
                    sb.append(line);
                    line = buffR.readLine();
                }
                sb.append("\n");

            } catch (IOException ioe) {
                System.out.println("nem talált");
            }
        }
        
        System.out.println(sb.toString());
    }

    public static int howManyFilesAre() {
        int counter = 1;
        boolean isIn = false;
        do {
            File file = new File("lottery_ticket_" + counter);
            if (file.isFile()) {
                counter++;
                isIn = true;
            } else {
                isIn = false;
            }
            
        } while (isIn);
        return counter;
    }
    

    public static void writeTicketInMap(int howMany) {
        for (int i = 1; i < howMany + 1; i++) {
            File file = new File("lottery_ticket_" + i);

            try (BufferedWriter buffW = new BufferedWriter(new FileWriter(file))) {

                StringBuilder sb = new StringBuilder();
                int[] generatedNumbers = lotteryNumbersGenerator();
                for (int number : generatedNumbers) {
                    sb.append(number);
                    sb.append(" ");
                }

                buffW.write(sb.toString());

            } catch (IOException ioe) {
                System.out.println("Nem sikerült, baszki");
            }

        }
    }

    public static String stringDateGenerator() {
        Date now = new Date();
        String depoPath = now.toString().replace(':', '_');
        return depoPath;
    }

    public static int scanUserInput() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Hány lottószelvényt készítsek?");
        int howMany = sc.nextInt();
        return howMany;
    }
}
