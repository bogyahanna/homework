/*
 Készítsen lottószelvény készítő programot. A program a felhasználó által 
 megadott darabszámú lottószelvényt generáljon, és mindet egy külön fájlba írja 
 ki olyan formátumban, hogy majd ezt később be is tudjuk olvasni.
 */
package lottery_ticket;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Lottery_ticket {

    public static void main(String[] args) {
        int howMany = scanUserInput();
        writeLotteryTicket(howMany);

    }

    public static void writeLotteryTicket(int howMany) {
        for (int i = 1; i < howMany + 1; i++) {
            File file = new File("lottery_ticket_" + i);

            try (BufferedWriter buffW = new BufferedWriter(new FileWriter(file))) {

                StringBuilder sb = new StringBuilder();
                int[] generatedNumbers = lotteryNumbersGenerator();
                for (int number : generatedNumbers) {
                    sb.append(number);
                    sb.append(" ");
                }
                
                buffW.write(sb.toString());
                
                
            } catch (IOException ioe) {
                System.out.println("Nem sikerült, baszki");
            }

        }
    }

    public static int scanUserInput() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Hány lottószelvényt készítsek?");
        int howMany = sc.nextInt();
        return howMany;
    }
    public static int[] lotteryNumbersGenerator() {
        int[] lotteryNumbers = new int[6];
        for (int i = 0; i < lotteryNumbers.length; i++) {
            int randomNumber = (int) (Math.random() * 90) + 1;

            boolean isInvalid = false;

            for (int j = 0; j < lotteryNumbers.length; j++) {
                if (randomNumber == lotteryNumbers[j]) {
                    isInvalid = true;
                }
            }
            if (!isInvalid) {
                lotteryNumbers[i] = randomNumber;
            } else {
                i--;
            }
        }
        return lotteryNumbers;
    }

}
