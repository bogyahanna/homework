package swing_file;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class ReadAndPrint extends JFrame {

    private File file = new File("quotes.txt");
    private final JTextArea text;
   
    public ReadAndPrint(String title) throws HeadlessException {
        super(title);
        text = new JTextArea();
        this.file = file;
    }

    public void init() {
        setSize(600, 300);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        setLayout(new BorderLayout());        
        add(text);
        

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = br.readLine();
            StringBuilder sb = new StringBuilder();
           
            while (line != null) {
                sb.append(line + "\n");
                line = br.readLine();                
            }
           
           text.setText(sb.toString()); //a label nem tud új sorokat fogadni?

        } catch (IOException ioe) {
            System.out.println("Hiba a fájl olvasása közben!");
        }
    }
}
