/*

 */
package running;

import java.util.Scanner;

public class Running {

    public static final Scanner sc = new Scanner(System.in);
    public static String name;
    public static int result;
    
    public static void main(String[] args) {
        int numberofRunners = numberOfCompetitors();
        

        String[] names = new String[numberofRunners];
        int[] results = new int[numberofRunners];

        for (int i = 0; i < names.length; i++) {
            System.out.println("Add meg a versenyző nevét: ");
            name = sc.next();
            names[i] = name;

            System.out.println("Add meg a teljesített időt: ");
            result = sc.nextInt();
            results[i] = result;
        }

        for (int i = 0; i < results.length; i++) {
            for (int j = i + 1; j < results.length; j++) {
                if (results[i] > results[j]) {
                    int temp = results[i];
                    results[i] = results[j];
                    results[j] = temp;
                    
                    String tempS = names[i];
                    names[i] = names[j];
                    names[j] = tempS;
                }
            }
        }

        for (int i = 0; i < 3; i++) {
            System.out.println((i + 1) + ". helyezett: " + names[i] + ". Idő: " + results[i]);
        }

    }

    public static int numberOfCompetitors() {
        int numberofRunners;
        System.out.println("Add meg a játékosok számát: ");
        do {
            numberofRunners = sc.nextInt();
        } while (numberofRunners <= 3);
        return numberofRunners;
    }
}

    
