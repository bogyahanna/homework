
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SiteDAO {

    private static final String CONNECTION_URL = "jdbc:mysql://localhost/car_database?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String INSERT_NEW_SITE = "INSERT INTO site (adress) VALUES (?)";

    public static void addBooking(HttpServletRequest request, HttpServletResponse response) throws IOException {

        try (Connection conn = DriverManager.getConnection(CONNECTION_URL, "admin", "admin");
                PreparedStatement ps = conn.prepareStatement(INSERT_NEW_SITE)) {

            String adress = request.getParameter("adress");

            ps.setString(1, adress);

            ps.executeUpdate();

            response.sendRedirect("MainMenuServlet");

            System.out.println("Sikeres");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
