
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CarDAO {

    private static final String CONNECTION_URL = "jdbc:mysql://localhost/car_database?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String INSERT_NEW_CAR = "INSERT INTO car (plate_number, color, engine_type, weight, year_of_built, site_id) VALUES (?, ?, ?, ?, ?, ?)";

    public static void addCar(HttpServletRequest request, HttpServletResponse response) throws IOException {

        try (Connection conn = DriverManager.getConnection(CONNECTION_URL, "admin", "admin");
                PreparedStatement ps = conn.prepareStatement(INSERT_NEW_CAR)) {

            String plate_number = request.getParameter("plate_number");
            String color = request.getParameter("color");
            int engine_type = Integer.valueOf(request.getParameter("engine_type"));
            int weight = Integer.valueOf(request.getParameter("weight"));
            int year_of_built = Integer.valueOf(request.getParameter("year_of_built"));
            int site_id = Integer.valueOf(request.getParameter("id"));

            ps.setString(1, plate_number);
            ps.setString(2, color);
            ps.setInt(3, engine_type);
            ps.setInt(4, weight);
            ps.setInt(5, year_of_built);
            ps.setInt(6, site_id);

            ps.executeUpdate();

            response.sendRedirect("MainMenuServlet");

            System.out.println("Sikeres");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
