<%-- 
    Document   : car
    Created on : Jun 16, 2020, 7:29:17 PM
    Author     : Tamás
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <h1> Car </h1>
        <br />
        <form action="CarServlet" method="post">

            <label for="plate_number"><b>Plate Nr.</b></label>
            <input type="text" name="plate_number" required>
            <br />
            <label for="color"><b>Color</b></label>
            <input type="text" name="color" required>
            <br />
            <label for="engine_type"><b>Engine Type</b></label>
            <input type="text" name="engine_type" required>
            <br />
            <label for="weight"><b>Weight</b></label>
            <input type="text" name="weight" required>
            <br />
            <label for="year_of_built"><b>Year of built</b></label>
            <input type="text" name="year_of_built" required>
            <br />

            <label for="site_id">Choose a site:</label>
            <select name="id" id="id">
                <option value="1">Bangkok</option>
                <option value="2">Budapest</option>
                <option value="3">Kraszna</option>
                <option value="4">Bukarest</option>
                <option value="5">Tel-Aviv</option>
            </select>
            <br><br>
           
            <input type="submit" value="Submit">
        </form>
    </body>
</html>
