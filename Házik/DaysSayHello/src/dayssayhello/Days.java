/*Készíts egy napok ENUM-ot, ahol minden nap lehet egy sayHello üzenetet
 kiíratni, ami minden napnál más, és az átlagos aktuális hangulatot képviseli.*/
package dayssayhello;

public enum Days {

    SUNDAY("crying"),
    MONDAY("oh no"),
    TUESDAY("ugh"),
    WEDNESDAY("why"),
    THURSDAY("OMG"),
    FRIDAY("finally"),
    SATURDAY("yees");

    private String feeling;

    public String getFeeling() {
        return feeling;
    }

    private Days(String feeling) {
        this.feeling = feeling;
    }

    public static void sayHello(Days d) {
        System.out.println("Hello, it's " + d + "! - " + d.getFeeling());

    }

}
