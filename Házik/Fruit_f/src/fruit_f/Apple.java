
package fruit_f;

public class Apple extends Fruits{
    private String taste;
    private int price = 450;
    
    public Apple(double weight, String colour, String taste) {
        super(weight, colour);
        this.taste = taste;
        
    }

    public String getTaste() {
        return taste;
    }

    public void setTaste(String taste) {
        this.taste = taste;
    }
    
    public int getPrice() {
    return (int) super.getWeight() * this.price;
    
    }
        
    @Override
    public String toString() {
        return "Your apple tastes like: " + this.taste + " and the price of " + super.getWeight() + "kg is: " + getPrice();
    }
    
    
    
}
