/*
 Legyen egy Fruit osztalyod. Minden Fruitnak van valamilyen szine es 
 kilogrammonkenti ara. Ebbol szarmaztass le egy Banana, Apple, Orange 
 osztalyt. A Banana osztaly specialis, mert annak mindig a dekagrammonkenti 
 arat szeretnenk megtudni. Az Applenek van iz tipusa is. Pl lehet fanyar, edes,
 stb.. egy Apple. Az Orange mindig naranccsarga szinu.

 */
package fruit_f;

public class Fruit_f {

    public static void main(String[] args) {
        Fruits alma = new Apple(3, "red", "sweet");
        Fruits banan = new Banana(400, "yellow");
        Fruits narancs = new Orange(4.5, null);

        System.out.println(alma.toString());
        System.out.println(narancs.toString());
        System.out.println(banan.toString());

    }

}
