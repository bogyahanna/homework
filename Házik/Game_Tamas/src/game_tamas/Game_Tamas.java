/*
van benne 5 bomba és 1 kincs
*/

package game_tamas;

import java.util.Scanner;

public class Game_Tamas {
    public static final int FIELD_SIZE = 10;

    public static final int PLAYER = 1;
    public static final int EMPTY = 0;
    public static final int WALL = -1;
    public static final int GATE = 2;
    public static final int BOMB = 3;
    public static final int TREASURE = 4;

    public static final char PLAYER_CHAR = 'p';
    public static final char EMPTY_CHAR = '.';
    public static final char WALL_CHAR = '*';
    public static final char GATE_CHAR = 'g';
    public static final char BOMB_CHAR = 'B';
    public static final char TREASURE_CHAR = 'T';

    public static final char UP = 'w';
    public static final char LEFT = 'a';
    public static final char DOWN = 's';
    public static final char RIGHT = 'd';
    public static final char EXIT = 'x';

    public static final Scanner sc = new Scanner(System.in);

    public static int[][] field = new int[FIELD_SIZE][FIELD_SIZE];
    public static int playerX;
    public static int playerY;
    
    public static int gateX;
    public static int gateY;
    
    public static int bombX;
    public static int bombY;
    
     public static int treasureX;
    public static int treasureY;
    
    public static int counter = 0;
    public static int treasureCounter = 0;

    public static void main(String[] args) {
        initField();
                
        play();
    }

    public static void play() {
        printField();
        
        System.out.print("kérem adja be a menetirányt (w, a, s, d, x): ");
        char ch;
        do {
            ch = sc.next().charAt(0);//bekérünk egy karaktert

            switch (ch) {
                case UP:
                    handleMove(playerX, playerY-1); break;
                case DOWN:
                    handleMove(playerX, playerY+1); break;
                case LEFT:
                    handleMove(playerX-1, playerY); break;
                case RIGHT:
                    handleMove(playerX+1, playerY); break;
            }
            printField();
        } while(ch != EXIT && !checkWin() && !checkLoss());
    }
    
    public static boolean checkWin() {
        if(playerX == gateX && playerY == gateY && treasureCounter == 1) {
            System.out.println("Gratulálunk! " + counter + " lépésben sikeresen kijutott!");
            return true;
                    
        }
        return false;
    }
    
    public static boolean checkLoss() {
        if(playerX == bombX && playerY == bombY) {
            System.out.println("Ezt megszívtad! ");
            return true;
        }
        return false;
    }
    
    
    public static void handleMove(int newPlayerX, int newPlayerY) {
        if(!validateMove(newPlayerX, newPlayerY)) {
            System.out.println("Falba ütközött");
            return;//visszater a metodusbol, kb. mint ciklusnal a break
        }
        if (field[playerY][playerX] == field[treasureY][treasureX]) {
        treasureCounter++;
            System.out.println("Megtaláltad a kincset, irány a kijárat!");
        }

        setPlayer(newPlayerX, newPlayerY);
        counter++;
    }
    
    public static void setPlayer(int newPlayerX, int newPlayerY) {
        clearPlayer();
        playerX = newPlayerX;
        playerY = newPlayerY;
        field[playerY][playerX] = PLAYER;
    }
    
    public static void clearPlayer() {
        
        field[playerY][playerX] = EMPTY;
    }
    
    public static boolean validateMove(int newPlayerX, int newPlayerY) {
        if(newPlayerX<0 || newPlayerX>=FIELD_SIZE || newPlayerY<0 || newPlayerY>=FIELD_SIZE) {
            return false;
        }
        
        return true;
    }
    
    

    public static void initField() {
        int[] tmp = generateCoordinate();
        playerX = tmp[0];
        playerY = tmp[1];
        field[playerY][playerX] = PLAYER;
        
        tmp = generateCoordinate();
        gateX = tmp[0];
        gateY = tmp[1];
        field[gateY][gateX] = GATE;
        
        tmp = generateCoordinate();
        treasureX = tmp[0];
        treasureY = tmp[1];
        field[treasureY][treasureX] = TREASURE;
        
       
        for (int i = 0; i < 5; i++) {
        tmp = generateCoordinate();
        bombX = tmp[0];
        bombY = tmp[1];
        field[bombY][bombX] = BOMB;
        }
    }
    
    public static int[] generateCoordinate() {
        
        int x = (int) (Math.random() * FIELD_SIZE);
        int y = (int) (Math.random() * FIELD_SIZE);
        
        if(field[x][y] != EMPTY) {
            return generateCoordinate();
        }
        
        int[] ret = new int[2];
        ret[0] = x;
        ret[1] = y;
        
        return ret;
    }

    public static void printField() {
        printWallLine();
        for (int i = 0; i < field.length; i++) {
            System.out.print(WALL_CHAR);
            for (int j = 0; j < field[i].length; j++) {
                switch(field[i][j]) {
                    case PLAYER: System.out.print(PLAYER_CHAR);break;
                    case EMPTY: System.out.print(EMPTY_CHAR);break;
                    case GATE: System.out.print(GATE_CHAR);break;
                    case BOMB: System.out.print(BOMB_CHAR);break;
                    case TREASURE: System.out.print(TREASURE_CHAR);break;
                }
            }
            System.out.print(WALL_CHAR);
            System.out.println("");
        }
        printWallLine();
    }

    public static void printWallLine() {
        for (int i = 0; i < field[0].length + 2; i++) {
            System.out.print(WALL_CHAR);
        }
        System.out.println("");
    }

    
}
